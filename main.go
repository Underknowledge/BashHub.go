package main

import (
	"bufio"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"os/exec"
	"strings"
	"time"

	"golang.org/x/time/rate"
)


type Response struct {
	Output   string `json:"output,omitempty"`
	Error    string `json:"error,omitempty"`
	ExitCode int    `json:"exit_code"`
}

func main() {
	configFilePath := os.Getenv("CONFIG")
	if configFilePath == "" {
		configFilePath = "paths.conf"
	}

	configMap := make(map[string]string)
	configFile, err := os.Open(configFilePath)
	if err != nil {
		fmt.Println("Error opening config file:", err)
		os.Exit(1)
	}
	defer configFile.Close()

	reader := bufio.NewReader(configFile)
	for {
		line, err := reader.ReadString('\n')
		if err == io.EOF {
			break
		}
		if err != nil {
			fmt.Println("Error reading config file:", err)
			os.Exit(1)
		}
		line = strings.TrimSpace(line)
		if len(line) == 0 || line[0] == '#' {
			continue
		}
		parts := strings.SplitN(line, ":", 2)
		if len(parts) != 2 {
			fmt.Println("Invalid line in config file:", line)
			continue
		}
		path, command := parts[0], parts[1]
		configMap[path] = command
	}

	listenAddress := os.Getenv("LISTEN_ADDRESS")
	if listenAddress == "" {
		listenAddress = ":8088"
	}

	logFile, err := os.OpenFile("error.log", os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		fmt.Println("Error opening log file:", err)
		os.Exit(1)
	}
	defer logFile.Close()
	logger := log.New(logFile, "", log.LstdFlags)

	intervalStr := os.Getenv("RATE_LIMIT_INTERVAL")
	if intervalStr == "" {
		intervalStr = "3s"
	}
	interval, err := time.ParseDuration(intervalStr)
	if err != nil {
		interval = 3 * time.Second
	}
	limiter := rate.NewLimiter(rate.Every(interval), 1)

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		if !limiter.Allow() {
			http.Error(w, "Rate limit exceeded", http.StatusTooManyRequests)
			return
		}

		// associate command with path
		command, ok := configMap[r.URL.Path]
		if !ok {
			logger.Printf("Host: %s Status: 501 Reason: Invalid path %s", r.RemoteAddr, r.URL.Path)
			response := Response{Error: "Invalid path", ExitCode: 501}
			responseJSON, err := json.MarshalIndent(response, "", "\t")
			if err != nil {
				http.Error(w, fmt.Sprintf("Error marshalling response: %v", err), http.StatusInternalServerError)
				return
			}
			w.Header().Set("Content-Type", "application/json")
			w.WriteHeader(http.StatusNotImplemented)
			w.Write(responseJSON)
			return
		}
		fmt.Printf("Host: %s Path: %s\n", r.RemoteAddr, r.URL.Path)

		cmd := exec.Command("bash", "-c", command)
		out, err := cmd.CombinedOutput() // Added CombinedOutput() to get stderr output
		if err != nil {
			// HERE
			errStr := fmt.Sprintf("Error running command: %v", err)                                         // Added variable to store stderr
			response := Response{Error: errStr, Output: string(out), ExitCode: cmd.ProcessState.ExitCode()} // Added output to the Response struct
			responseJSON, err := json.MarshalIndent(response, "", "\t")
			if err != nil {
				http.Error(w, fmt.Sprintf("Error marshalling response: %v", err), http.StatusInternalServerError)
				return
			}
			w.Header().Set("Content-Type", "application/json")
			w.Write(responseJSON)
			return
		}
		response := Response{Output: string(out), ExitCode: cmd.ProcessState.ExitCode()}
		responseJSON, err := json.MarshalIndent(response, "", "\t")
		if err != nil {
			http.Error(w, fmt.Sprintf("Error marshalling response: %v", err), http.StatusInternalServerError)
			return
		}
		w.Header().Set("Content-Type", "application/json")
		w.Write(responseJSON)
	})

	fmt.Printf("Listening on address %s...\n", listenAddress)
	http.ListenAndServe(listenAddress, nil)
}
