# bashHub.go

Bring up a simple REST API that listens for incoming HTTP GET requests 
and run specified shell commands when a request is received. 
Paths and commands configurable via a config file.
While this technicaly counts as remote code execution, it should be fine ⟨™⟩ 


Typical case of, [oh, something like this exists already.. shit..](https://www.olivetin.app/) well... now I'am already finished.
also I think that I could expose this to the mean wide net without geting into to many troubles.. Famous last words Ladies and Gents! 


##  Configuration

Some settings can be changed with Environment variables

`CONFIG`: Path to the config file. Default is `paths.conf`.
`LISTEN_ADDRESS`: Address to listen on. Default is `:8088` (mind the `:`).
`RATE_LIMIT_INTERVAL`: Interval for rate limiting in seconds. Can be anything for example `500ms` or `2h30m`. Default is `3s`. 


an example `CONFIG`/`paths.conf` could look like this:

```conf
/ping: echo pong
/thisisaverysecretpath/restart-plex: cd /opt && docker-compose restart plex
/anothersecretpath/shutdown-system: shutdown -h now
/echo/echo: echo "Oh yea"
/date123: echo $(date) >> /tmp/123
/false: false ; echo "No problem, that was on purpose"
/ipa: ip a 
/iptmp: ip a | tee -a /tmp/ipa
```
the first part is the path, it has to start with a `/` and afterwards are the commands.  
redirection, and pipes are apperantly working, but I didnt tested exstensivly.



## Usage

Send a GET request to the API with the desired path set in `paths.conf`. For example, to run the restart-plex command:

```
curl http://localhost:8080/thisisaverysecretpath/restart-plex
```

The API will run the corresponding command and return the output.
when you want to get a decent output you can pipe it to jq like this 
`curl localhost:8088/ipa | jq -r .output`

I'm happy to get pull requests when you think that this should be improved! 


## systemd 

```bash
scp bashHub.go ${TARGET}/usr/local/bin/bashHub.go
``` 


```bash
# Create the bash-hub user and group with the GID 54321
groupadd --gid 54321 bash-hub
useradd --gid 54321 --create-home --home-dir /home/bash-hub bash-hub

# Set up the Bash Hub API as a systemd service
cat > /etc/systemd/system/bash-hub.service << EOF
[Unit]
Description=Bash Hub API

[Service]
ExecStart=/usr/local/bin/bashHub.go
Environment="LISTEN_ADDRESS=:8123"
Environment="CONFIG=/home/bash-hub/bashHub.conf"
Restart=always
User=bash-hub
Group=bash-hub

[Install]
WantedBy=multi-user.target
EOF

cat > /home/bash-hub/bashHub.conf << EOF
/ping: echo "pong"
/ha: qm reset 100
/reset: reboot -h now
/update: apt-get update && apt-get upgrade -y && reboot -h now
EOF

chown bash-hub:bash-hub /home/bash-hub/bashHub.conf
chmod 600 /home/bash-hub/bashHub.conf


systemctl daemon-reload
systemd-analyze verify /etc/systemd/system/bash-hub.service && systemctl enable --now bash-hub

systemctl status bash-hub
```



## Limitations
No errors handeling or unexpected output from the command what so ever.


## Building

```
go get golang.org/x/time/rate
CGO_ENABLED=0 go build -o bashHub.go
```


## License
This project is licensed under the MIT License. See the LICENSE file for details.